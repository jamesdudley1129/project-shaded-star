
/* IGNORE
ERROR: Error while trying to enter break state. Debugging will now stop. Failed to find thread 1 for break event
The program 'e:\Projects\C projects\VScode\Test 0\main.exe' has exited with code 42 (0x0000002a).
   IGNORE 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
//#include <InputScope.h>
/*#include <windows.h>

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE _, PWSTR pCmdLine, int nCmdShow)
{
    // Register the window class.
    const wchar_t CLASS_NAME[]  = L"Sample Window Class";
    
    WNDCLASS wc = { 0 };

    wc.lpfnWndProc   = WindowProc;
    wc.hInstance     = hInstance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    // Create the window.

    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles.
        CLASS_NAME,                     // Window class
        L"Learn to Program Windows",    // Window text
        WS_OVERLAPPEDWINDOW,            // Window style

        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
        );

    if (hwnd == NULL)
    {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);

    // Run the message loop.

    MSG msg = { 0 };
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);



            FillRect(hdc, &ps.rcPaint, (HBRUSH) (COLOR_WINDOW+1));

            EndPaint(hwnd, &ps);
        }
        return 0;

    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

typedef struct vec3
{
    float e[3];
} vec3_t;

vec3_t vec3 (float x, float y, float z)
{
    vec3_t v;
    v.e[0] = x;
    v.e[1] = y;
    v.e[2] = z;
    return v;
}

#define DEFINE_VEC3_OPERATOR(OPERATOR_NAME, OPERATOR) \
vec3_t OPERATOR_NAME ## _vec3 (vec3_t a, vec3_t b) \
{ \
    vec3_t dst; \
    dst.e[0] = a.e[0] OPERATOR b.e[0]; \
    dst.e[1] = a.e[1] OPERATOR b.e[1]; \
    dst.e[2] = a.e[2] OPERATOR b.e[2]; \
    return dst; \
}

DEFINE_VEC3_OPERATOR(add, +)
DEFINE_VEC3_OPERATOR(sub, -)
DEFINE_VEC3_OPERATOR(mul, *)
DEFINE_VEC3_OPERATOR(div, /)
*/

//all Complex Function Tags
struct Primary;
struct Secondary;
struct Ship;
struct Inventory;
struct Charicter;
struct Enamy;
struct Player;

//primarys have more ammo but do less damage then secondarys
struct Primary
{
    int dmg;
    int ammo;
};
//Secondarys do more damage a example is a grenade but have fewer rounds(ammo);
struct Secondary
{
    int dmg;
    int ammo;
};

struct Ship
{
    int invX;
    int invY;
    int invZ;
    bool isAtmosphere;
    struct Primary MainGun;
    struct Secondary SecGun;
    int shield;
    int hull;
};

struct Inventory
{
    int spaceX;
    int spaceY;
    int spaceZ;
};
struct Character
{
    char name[30];
    float money;
    struct Inventory inv;
    double health;
    struct Primary gun;
    struct Secondary Grenade;
    struct Ship ship;
    bool hasShip;
};
struct Player
{
   struct Character character;
};

struct Enamy
{
    int level;
    //level helps as a modifier when defineing its Character
    struct Character character;
};




//declared functions
struct Player creatUser();
void mainMenu();
void Credits();



int main (void)
{
    Credits();
    mainMenu();

#if defined( _WIN32 )
    system("pause");
#elif defined( __unix__ )
    system("read -n1 -r -p \"Press any key to continue...\" key");
#endif

    return 0;
}

void Credits()
{
    const char *Dev = "James";
    printf("Developed by %s. \n", Dev);
    printf("A story game...\n");
    printf("'Project Shaded Star\n\n'");
    
}

struct Player creatUser(){
    
    printf("Please enter your name!(under 30 letters):\n ");
    char name[30];
    scanf("%s", name);
    struct Player player;
    //setting players stats//
    strcpy(player.character.name, name);
    player.character.money = 0;
    player.character.health = 100;
    player.character.gun.ammo = 100;
    player.character.gun.dmg = 1.2;
    player.character.Grenade.dmg = 10;
    player.character.Grenade.ammo = 4;
    player.character.inv.spaceX = 0;
    player.character.inv.spaceY = 0;
    player.character.inv.spaceZ = 0;
    player.character.ship.hull = 1;
    player.character.ship.invX = 0;
    player.character.ship.invY = 0;
    player.character.ship.invZ = 0;
    player.character.ship.isAtmosphere = true;
    player.character.ship.MainGun.ammo = 0;
    player.character.ship.MainGun.dmg = 0;
    player.character.ship.SecGun.ammo = 0;
    player.character.ship.SecGun.dmg = 0;
    player.character.ship.shield = 0;
    player.character.hasShip = false;
    return player;
}


void mainMenu()
{    
    //if file != null load if load succeeds apply to player else creat new player
    struct Player player;
    player = creatUser();

    printf("welcome %s Would you like to play the tutorial? \n",player.character.name);
    char input[1];
    scanf("%s",input);
    if(input[0] == "y"){
        //play tutorial
    }
}

//new game start
